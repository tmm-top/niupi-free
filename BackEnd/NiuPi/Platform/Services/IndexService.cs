﻿using Furion.DataEncryption;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NiuPi.Database.Platform;
using NiuPi.Tools;
using SqlSugar;

namespace NiuPi.Platform.Services
{
    /// <summary>
    /// 基础服务模块
    /// </summary>
    [Route("api/platform/[controller]")]
    [ApiDescriptionSettings("platform")]
    public class IndexService : IDynamicApiController, ITransient
    {
        private readonly ILogger<IndexService> Logger;
        private readonly SqlSugarScope Db;
        public IndexService(ILogger<IndexService> _Logger, SqlSugarScope _Db)
        {
            Logger = _Logger;
            Db = _Db;
        }
        /// <summary>
        /// 系统信息
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<IActionResult> Get()
        {
            var info = await MachineUtil.GetMachineBaseInfo();
            return RestfulResult.Instance.OnSucceeded(info);
        }
        /// <summary>
        /// 初始化数据库,先将所有表删除，在初始化，此操作不可恢复，请慎重
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> InitDatabase()
        {
            Db.DbMaintenance.CreateDatabase();
            List<string> tables = new List<string>() { "menu", "role", "rolepermission", "user" };
            foreach (var table in tables)
            {
                if (Db.DbMaintenance.IsAnyTable(table, false))
                {
                    Db.DbMaintenance.DropTable(table);
                }
            }
            Db.CodeFirst.InitTables(typeof(User), typeof(Menu), typeof(Role), typeof(RolePermission));
            return RestfulResult.Instance.OnSucceeded(default, "数据库初始化成功");
        }
    }
}