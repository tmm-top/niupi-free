﻿namespace NiuPi.Platform.Entity
{
    public class UserInfo
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 租户ID  在此作为门店ID
        /// </summary>
        public long? TenantId { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string Account { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string? NickName { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string? Avatar { get; set; }
        /// <summary>
        /// 权限标识
        /// </summary>
        public string[] Permissions { get; set; }
        /// <summary>
        /// access_token
        /// </summary>
        public string AccessToken { get; set; }
    }
}
