﻿namespace NiuPi.Platform.Entity
{
    public class MenuTree
    {
        /// <summary>
        /// ID
        /// </summary>
        public long Id { get; set; } = 0;
        /// <summary>
        /// 菜单父级ID
        /// </summary>
        public long? ParentID { get; set; } = 0;
        /// <summary>
        /// 菜单类型（0目录 1菜单 2按钮）
        /// </summary>
        public int? Type { get; set; } = 0;
        /// <summary>
        /// 菜单名称
        /// </summary>
        public string? MenuName { get; set; }
        /// <summary>
        /// 前端路由
        /// </summary>
        public string? RoutePath { get; set; }
        /// <summary>
        /// 前端权限标识
        /// </summary>
        public string? Permission { get; set; }
        /// <summary>
        /// 前端组件路径
        /// </summary>
        public string? AssemblyPath { get; set; }
        /// <summary>
        /// 菜单图标
        /// </summary>
        public string? Icon { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int? Sort { get; set; } = 0;
        /// <summary>
        /// 当前状态
        /// </summary>
        public int? Status { get; set; } = 0;
        /// <summary>
        /// 子树结构
        /// </summary>
        public List<MenuTree> Children { get; set; } = new List<MenuTree>();
    }
    public class MenuMeta
    {
        public string title { get; set; }
        public bool isKeepAlive { get; set; }
        public bool isAffix { get; set; }
        public string[] auth { get; set; }
        public string icon { get; set; }
    }
    public class RouteMenu
    {
        public string name { get; set; }
        public string path { get; set; }
        public string component { get; set; }
        public string redirect { get; set; }
        public MenuMeta meta { get; set; } = new MenuMeta();
        public List<RouteMenuChild> children { get; set; } = new List<RouteMenuChild>();
    }
    public class RouteMenuChild
    {
        public string name { get; set; }
        public string path { get; set; }

        public string component { get; set; }
        public MenuMeta meta { get; set; } = new MenuMeta();

    }

}