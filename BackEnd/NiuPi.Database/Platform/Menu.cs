﻿using NiuPi.Database.BasicModel;
using SqlSugar;
namespace NiuPi.Database.Platform
{
    /// <summary>
    /// 菜单表
    /// </summary>
    [SugarTable("menu")]
    public class Menu : Base
    {
        /// <summary>
        /// 菜单父级ID
        /// </summary>
        [SugarColumn(ColumnDescription = "菜单父级ID", DefaultValue = "0", IsNullable = false)]
        public long? ParentID { get; set; } = 0;
        /// <summary>
        /// 菜单类型（0目录 1菜单 2按钮）
        /// </summary>
        [SugarColumn(ColumnDescription = "菜单类型（0目录 1菜单 2按钮）", DefaultValue = "0", IsNullable = false)]
        public int? Type { get; set; } = 0;
        /// <summary>
        /// 菜单名称
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "菜单名称", IsNullable = false)]
        public string? MenuName { get; set; }
        /// <summary>
        /// 前端路由
        /// </summary>

        [SugarColumn(Length = 255, ColumnDescription = "前端路由", IsNullable = false)]
        public string? RoutePath { get; set; }
        /// <summary>
        /// 前端权限标识
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "前端权限标识", IsNullable = false, UniqueGroupNameList = new string[] { "Permission" })]
        public string? Permission { get; set; }
        /// <summary>
        /// 前端组件路径
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "前端组件路径", IsNullable = false)]
        public string? AssemblyPath { get; set; }
        /// <summary>
        /// 菜单图标
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "菜单图标", IsNullable = false)]
        public string? Icon { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        [SugarColumn(ColumnDescription = "排序", DefaultValue = "0", IsNullable = false)]
        public int? Sort { get; set; } = 0;
    }
}