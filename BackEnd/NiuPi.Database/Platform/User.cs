﻿using NiuPi.Database.BasicModel;
using SqlSugar;

namespace NiuPi.Database.Platform
{
    /// <summary>
    /// 用户表
    /// </summary>
    [SugarTable("user")]
    public class User : Base
    {
        [SugarColumn(ColumnDescription = "是否超级管理员：0否，1是", DefaultValue = "0", IsNullable = false)]
        public int? AdminType { get; set; } = 0;
        /// <summary>
        /// 账号
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "账号", IsNullable = false, UniqueGroupNameList = new string[] { "Account" })]
        public string? Account { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "密码", IsNullable = false)]
        public string? Password { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "手机号", IsNullable = false, UniqueGroupNameList = new string[] { "Phone" })]
        public string? Phone { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "昵称", IsNullable = true)]
        public string? NickName { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "头像", IsNullable = true)]
        public string? Avatar { get; set; }
        /// <summary>
        /// 生日
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "生日", IsNullable = true)]
        public string? Birthday { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        [SugarColumn(ColumnDescription = "性别", DefaultValue = "0", IsNullable = false)]
        public int? Sex { get; set; } = 0;
        /// <summary>
        /// 邮箱
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "邮箱", IsNullable = true)]
        public string? Email { get; set; }
        /// <summary>
        /// 关联角色ID
        /// </summary>
        [SugarColumn(ColumnDescription = "关联角色ID", DefaultValue = "0", IsNullable = false)]
        public long? RoleId { get; set; } = 0;
        /// <summary>
        /// 关联店铺ID
        /// </summary>
        [SugarColumn(ColumnDescription = "关联店铺ID", DefaultValue = "0", IsNullable = false)]
        public long? StoreId { get; set; } = 0;
        /// <summary>
        /// 最后登录IP
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "最后登录IP", IsNullable = true)]
        public string? LastLoginIP { get; set; }
        /// <summary>
        /// 最后登录时间
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "最后登录时间", IsNullable = true)]
        public long? LastLoginTime { get; set; }
        [SugarColumn(IsIgnore = true)]
        public string RoleName { get; set; }
        [SugarColumn(IsIgnore = true)]
        public string TenantName { get; set; }
    }
    //此特性为分库的配置ID，根据此自动分库，暂时不用
    //[TenantAttribute("1")]
    //public class User2 : Base
    //{
    //    [SugarColumn(Length = 255, ColumnDescription = "姓名", IsNullable = true)]
    //    public string? Name { get; set; }
    //}
}