import { Module } from 'vuex';
// 此处加上 `.ts` 后缀报错，具体原因不详
import { ThemeConfigState, RootStateTypes } from '/@/store/interface/index';

/**
 * 2020.05.28 by lyt 优化
 * 修改一下配置时，需要每次都清理 `window.localStorage` 浏览器永久缓存，配置才会生效
 * 哪个大佬有解决办法，欢迎pr，感谢💕！
 */
const themeConfigModule: Module<ThemeConfigState, RootStateTypes> = {
	namespaced: true,
	state: {
		themeConfig: {
			"isDrawer": false,
			"primary": "#ED4C08",
			"success": "#67c23a",
			"info": "#909399",
			"warning": "#e6a23c",
			"danger": "#f56c6c",
			"topBar": "#ffffff",
			"menuBar": "#545c64",
			"columnsMenuBar": "#545c64",
			"topBarColor": "#606266",
			"menuBarColor": "#eaeaea",
			"columnsMenuBarColor": "#e6e6e6",
			"isTopBarColorGradual": false,
			"isMenuBarColorGradual": false,
			"isColumnsMenuBarColorGradual": false,
			"isMenuBarColorHighlight": false,
			"isCollapse": false,
			"isUniqueOpened": false,
			"isFixedHeader": false,
			"isFixedHeaderChange": true,
			"isClassicSplitMenu": false,
			"isLockScreen": false,
			"lockScreenTime": 30,
			"isShowLogo": true,
			"isShowLogoChange": false,
			"isBreadcrumb": true,
			"isTagsview": true,
			"isBreadcrumbIcon": true,
			"isTagsviewIcon": true,
			"isCacheTagsView": false,
			"isSortableTagsView": true,
			"isShareTagsView": false,
			"isFooter": true,
			"isGrayscale": false,
			"isInvert": false,
			"isIsDark": false,
			"isWartermark": false,
			"wartermarkText": "490912587@qq.com",
			"tagsStyle": "tags-style-one",
			"animation": "slide-right",
			"columnsAsideStyle": "columns-round",
			"columnsAsideLayout": "columns-vertical",
			"layout": "defaults",
			"isRequestRoutes": true,
			"globalTitle": "NiuPi服务平台",
			"globalViceTitle": "量子互联网络科技@li",
			"globalComponentSize": "mini"
		},
	},
	mutations: {
		// 设置布局配置
		getThemeConfig(state: any, data: object) {
			state.themeConfig = data;
		},
	},
	actions: {
		// 设置布局配置
		setThemeConfig({ commit }, data: object) {
			commit('getThemeConfig', data);
		},
	},
};

export default themeConfigModule;
