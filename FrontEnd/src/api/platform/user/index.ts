import request from '/@/utils/request';

export function getList(params: object) {
	return request({
		url: '/platform/user/list',
		method: 'get',
		params,
	});
}
export function add(params: object) {
	return request({
		url: '/platform/user',
		method: 'post',
		data: params
	});
}

export function edit(params: object) {
	return request({
		url: '/platform/user/edit',
		method: 'post',
		data: params
	});
}
export function set_role(params: object) {
	return request({
		url: '/platform/user/set-role',
		method: 'post',
		data: params
	});
}
export function bind_tenant(params: object) {
	return request({
		url: '/platform/user/bind-tenant',
		method: 'post',
		data: params
	});
}
export function changeStatus(params: object) {
	return request({
		url: '/platform/user/change-status',
		method: 'post',
		data: params
	});
}
export function change_pass(params: object) {
	return request({
		url: '/platform/user/change_pass',
		method: 'post',
		data: params
	});
}